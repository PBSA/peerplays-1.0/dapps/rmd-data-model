#

# Let's get this party started!
from wsgiref.simple_server import make_server

import os
import falcon
import json
import yaml

from model_db import ModelDatabase
from rmd_logic import Aggregates

class ServerAliveRPC:
    def on_get(self, req, resp):
        """Handles GET requests"""
        args = dict(req.params)
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
        resp.text = json.dumps("alive")

class ObjectCountsRPC:
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200  # This is the default status
        resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT

        rmddb = ModelDatabase.get_shared_instance()
        counts = rmddb.get_index_counts("by_type")
        resp.text = json.dumps(counts)

class GetIndexRPC:
    def on_get(self, req, resp, index_name, **kwargs):
        try:
            resp.status = falcon.HTTP_200  # This is the default status
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT

            rmddb = ModelDatabase.get_shared_instance()
            result = rmddb.get_index(index_name, **kwargs)
            resp.text = json.dumps(result)
        except ValueError as e:
            result = {
                'status': 'error',
                'error': str(e)
            }
            resp.status = falcon.HTTP_404
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)


class GetAggregateRPC:
    def on_get(self, req, resp, aggregate_name):

        try:
            resp.status = falcon.HTTP_200  # This is the default status
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT

            rmddb = ModelDatabase.get_shared_instance()
            agg = Aggregates(rmddb)
            result = agg.get_aggregate(aggregate_name)
            resp.text = json.dumps(result)
        except ValueError as e:
            result = {
                'status': 'error',
                'error': str(e)
            }
            resp.status = falcon.HTTP_404
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)


class GetObjectRPC:
    def on_get(self, req, resp, object_id):

        try:
            rmddb = ModelDatabase.get_shared_instance()
            result = rmddb.get_object(object_id)

            resp.status = falcon.HTTP_200  # This is the default status
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)
        except ValueError as e:
            result = {
                'status': 'error',
                'error': str(e)
            }
            resp.status = falcon.HTTP_404
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)


class GetWrappedObjectRPC:
    def on_get(self, req, resp, object_id):

        try:
            rmddb = ModelDatabase.get_shared_instance()
            result = rmddb.get_wrapped_object(object_id)

            resp.status = falcon.HTTP_200  # This is the default status
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)
        except ValueError as e:
            result = {
                'status': 'error',
                'error': str(e)
            }
            resp.status = falcon.HTTP_404
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)


class GetObjectsByTypeAndFieldRPC:
    def on_get(self, req, resp, object_type, field, value):

        try:
            rmddb = ModelDatabase.get_shared_instance()
            result = rmddb.get_objects_by_type_and_field(object_type, field, value)

            resp.status = falcon.HTTP_200  # This is the default status
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)
        except ValueError as e:
            result = {
                'status': 'error',
                'error': str(e)
            }
            resp.status = falcon.HTTP_404
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)


class GetUniqueValuesRPC:
    def on_get(self, req, resp, object_type, object_field):

        try:
            rmddb = ModelDatabase.get_shared_instance()
            result = rmddb.get_unique_values(object_type, object_field)

            resp.status = falcon.HTTP_200  # This is the default status
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)
        except ValueError as e:
            result = {
                'status': 'error',
                'error': str(e)
            }
            resp.status = falcon.HTTP_404
            resp.content_type = falcon.MEDIA_JSON if req.client_accepts_json else falcon.MEDIA_TEXT
            resp.text = json.dumps(result)

########

def LoadRMDDatabase():
    unit_test_data = {}
    with open("./unit-test-rmd-db.json") as in_file:
        unit_test_data = json.loads(in_file.read())
    mdb = ModelDatabase()
    mdb.ingest_rmd_collection(unit_test_data)



# falcon.App instances are callable WSGI apps...
# in larger applications the app is created in a separate file
app = falcon.App()

# Resources are represented by long-lived class instances
server_alive_rpc = ServerAliveRPC()
object_counts_rpc = ObjectCountsRPC()
get_aggregate_rpc = GetAggregateRPC()
get_index_rpc = GetIndexRPC()
get_object_rpc = GetObjectRPC()
get_wrapped_object_rpc = GetWrappedObjectRPC()
get_objects_by_type_and_field_rpc = GetObjectsByTypeAndFieldRPC()
get_unique_values_rpc = GetUniqueValuesRPC()

if __name__ == '__main__':
    working_dir = os.path.dirname(os.path.abspath(__file__))
    swagger_config = os.path.join(working_dir, 'conf/swagger.yaml')
    app_config = os.path.join(working_dir, 'conf/explorer-conf.yaml')
    config = yaml.safe_load(open(app_config, 'r'))

    def route_cat(route):
        # Add prefix to route if needed
        return '{}{}'.format(config["url-prefix"], route)

    app.add_route(route_cat('/health/alive'), server_alive_rpc)
    app.add_route(route_cat('/count/all_counts'), object_counts_rpc)
    app.add_route(route_cat('/aggregate/{aggregate_name}'), get_aggregate_rpc)
    app.add_route(route_cat('/index/{index_name}'), get_index_rpc)
    app.add_route(route_cat('/object/get/{object_id}'), get_object_rpc)
    app.add_route(route_cat('/object/get_wrapped/{object_id}'), get_wrapped_object_rpc)
    app.add_route(
        route_cat('/object/get_many/of_type/{object_type}/require/{field}/{value}'),
        get_objects_by_type_and_field_rpc
    )
    app.add_route(
        route_cat('/analysis/get_unique_values/{object_type}/{object_field}'),
        get_unique_values_rpc
    )

    from swagger_ui import falcon_api_doc
    docs_url = route_cat('/docs')
    falcon_api_doc(app, config_path=swagger_config, url_prefix=docs_url, editor=True)

    LoadRMDDatabase()

    with make_server(config["listen-host"], config["listen-port"], app) as httpd:
        print(
            'Serving on host %s port %d...' %
            (config["listen-host"],config["listen-port"])
        )
        print(
            'Swagger-UI at: http://%s:%d%s' %
            (config["listen-host"],config["listen-port"],docs_url)
        )
        # Serve until process is killed
        httpd.serve_forever()
