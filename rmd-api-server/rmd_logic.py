# Provides more in-depth interpretive analytics of RMD data.
# Initialize with a reference to an existing ModelDatabase
#


class Analysis:
    pass

class Aggregates:

    def __init__(self, mdb_instance):
        self.mdb = mdb_instance


    @staticmethod
    def _AmountFromCurrencyString(currency_str):
        # TODO: Do this right
        cur = currency_str.split(' ')[0]
        cur = "".join(cur.split(','))
        return float(cur)

    def GGRAggregate(self, object_ids):
        ggr_sum = 0
        for obj_id in object_ids:
            obj = self.mdb.get_object(obj_id)
            contrib = obj["report_details"]["gross_gaming_revenue"]
            contrib_val = Aggregates._AmountFromCurrencyString(contrib)
            ggr_sum += contrib_val
        return ggr_sum


    def ExpenseAggregate(self, object_ids):
        # Hacky
        # TODO: Fix
        ggr_sum = 0
        for obj_id in object_ids:
            obj = self.mdb.get_object(obj_id)
            contrib = obj["report_details"]["gross_gaming_revenue"]
            contrib_val = Aggregates._AmountFromCurrencyString(contrib)
            ggr_sum += contrib_val
            reducer = obj["report_details"]["net_gaming_revenue"]
            reducer_val = Aggregates._AmountFromCurrencyString(reducer)
            ggr_sum -= reducer_val
        return ggr_sum


    @staticmethod
    def get_dict_path(d, path=[]):
        result = d
        for key in path:
            result = result[key]
        return result

    @staticmethod
    def dict_currency_accumulate(accumulator, accumulus):
        if isinstance(accumulus, str):
            Aggregates.dict_currency_accumulate_string(accumulator, accumulus, "uncategorized")
        elif isinstance(accumulus, dict):
            Aggregates.dict_currency_accumulate_dict(accumulator, accumulus)
        else:
            ## TODO: handle
            pass

    @staticmethod
    def dict_currency_accumulate_string(accumulator, accumulus, category):
        assert isinstance(accumulus, str)
        amount = Aggregates._AmountFromCurrencyString(accumulus)
        accumulator[category] = accumulator.get(category, 0) + amount

    @staticmethod
    def dict_currency_accumulate_dict(accumulator, accumulus):
        assert isinstance(accumulus, dict)
        for key in accumulus.keys():
            if isinstance(accumulus[key], str):
                Aggregates.dict_currency_accumulate_string(accumulator, accumulus[key], key)
            elif isinstance(accumulus, dict):
                # This will flatten, which may be undesirable:
                Aggregates.dict_currency_accumulate_dict(accumulator, accumulus[key])
            else:
                pass # ignore if not string

    ###
    ##
    #  Aggregates currency values located a a given path over supplied objects
    #
    def PathCurrencyAggregate(self, object_ids, path):
        ag_breakdown = {}
        for obj_id in object_ids:
            obj = self.mdb.get_object(obj_id)
            contrib = Aggregates.get_dict_path(obj, path)
            Aggregates.dict_currency_accumulate(ag_breakdown, contrib)
        ag_total = 0
        for key in ag_breakdown.keys():
            ag_total += ag_breakdown[key]
        return {
            "total": ag_total,
            "breakdown": ag_breakdown
        }


    def get_aggregate(self, ag_type, report_ids = []):
        assert isinstance(ag_type, str)
        if len(report_ids) == 0:
            # TEMP: TODO: remove, this is for testing
            report_ids = self.mdb.get_index("by_type")["game_end_report"]
        if ag_type == "gross_gaming_revenue":
            #aggval = self.GGRAggregate(report_ids)
            ag_ret = self.PathCurrencyAggregate(report_ids, ["report_details","ggr_breakdown"])
        elif ag_type == "operating_expenses":
            #aggval = self.ExpenseAggregate(report_ids)
            ag_ret = self.PathCurrencyAggregate(report_ids, ["report_details","gr_reducers"])
        else:
            raise ValueError("Don't know how to aggregate: %s"%ag_type)
        return {
            'aggregate': ag_type,
            #'value': "{:.2f}".format(aggval),
            'value': ag_ret,
            'included_reports': len(report_ids)
        }


if __name__ == "__main__":
    # If invoked directly, run tests:
    import json
    from model_db import ModelDatabase

    unit_test_data = {}
    with open("./unit-test-rmd-db-01.json") as in_file:
        unit_test_data = json.loads(in_file.read())

    mdb = ModelDatabase()

    mdb.ingest_rmd_collection(unit_test_data)
    bti = mdb.get_index("by_type")

    agg = Aggregates(mdb)

    print(agg.GGRAggregate(bti["game_end_report"]))
    print(agg.ExpenseAggregate(bti["game_end_report"]))

