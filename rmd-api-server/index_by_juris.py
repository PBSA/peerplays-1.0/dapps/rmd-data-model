#

class IndexBase:

    def __init__(self):
        self._index = {}


    def ingest(self, wrapped_object):
        if self.selection_criteria(wrapped_object):
            index_keys = self.determine_index_keys(wrapped_object)
            for key in index_keys:
                self._index.setdefault(
                    key, []
                ).append(
                    wrapped_object["object_id"]
                )


    def get_index(self):
        return self._index

    def get_filtered_index(self, filter_spec):
        ## TODO: Apply the filter
        return self._index



###
##
#  This one accepts objects of type 'game_end_report' and indexes them by
#  object.details.jurisdiction.
#
class JurisdictionIndex(IndexBase):

    def __init__(self):
        super().__init__()


    def selection_criteria(self, wrapped_object):
        return wrapped_object["object_type"] == "game_end_report"


    def determine_index_keys(self, wrapped_object):
        return [
            wrapped_object["object"]["target_jurisdiction"]
        ]


class ObjectTypeIndex(IndexBase):

    def __init__(self):
        super().__init__()


    def selection_criteria(self, wrapped_object):
        return True


    def determine_index_keys(self, wrapped_object):
        return [
            wrapped_object["object_type"]
        ]


###
##
#  This is very much a work-in-progress... don't trust it yet.
#  One known problem is the lists in the index are not de-duplicated.
#
class BackLinkIndex(IndexBase):
    def __init__(self):
        super().__init__()


    def _is_obj_id(obj_id_maybe):
        assert isinstance(obj_id_maybe, str)
        ## This is not sophisticated enough but we'll use it for now.
        ## TODO: Do this right
        return obj_id_maybe.startswith("bafk")


    def _find_all_obj_ids(obj_tree, depth_limit = 100):
        retval = []
        if depth_limit < 1:
            return retval
        if isinstance(obj_tree, dict):
            for key in obj_tree.keys():
                retval.extend(
                    BackLinkIndex._find_all_obj_ids(obj_tree[key], depth_limit-1)
                )
        elif isinstance(obj_tree, list):
            for item in obj_tree:
                retval.extend(
                    BackLinkIndex._find_all_obj_ids(item, depth_limit-1)
                )
        elif isinstance(obj_tree, str):
            if BackLinkIndex._is_obj_id(obj_tree):
                retval.append(obj_tree)
        else:
            pass # ignore all other types
        return retval


    def selection_criteria(self, wrapped_object):
        return True


    def determine_index_keys(self, wrapped_object):
        return BackLinkIndex._find_all_obj_ids(wrapped_object["object"])
