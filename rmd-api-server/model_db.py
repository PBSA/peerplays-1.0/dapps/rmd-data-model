#
from index_by_juris import JurisdictionIndex, ObjectTypeIndex, BackLinkIndex

class ModelDatabase:

    _shared_instance = None

    def __init__(self):

        self.object_store = {}
        self.indices = {
            "by_type": ObjectTypeIndex(),
            "by_jurisdiction": JurisdictionIndex(),
            "by_back_link": BackLinkIndex()
        }

        if not self._shared_instance:
            ModelDatabase._shared_instance = self

    @classmethod
    def get_shared_instance(cls):
        if cls._shared_instance:
            return cls._shared_instance
        else:
            raise RuntimeError("Model Database not initialized.")


    def ingest_rmd_object(self, wrapped_object):
        if not (wrapped_object["data_type"] == "RMD_DATA_OBJECT"):
            raise ValueError("Not an RMD Object")
        assert isinstance(wrapped_object["object_id"], str)
        assert isinstance(wrapped_object["object_type"], str)
        assert isinstance(wrapped_object["object"], object)

        if not wrapped_object["object_id"] in self.object_store.keys():
            ## TODO: Check object ID matches
            self.object_store[wrapped_object["object_id"]] = (
                wrapped_object["object_type"],
                wrapped_object["object"]
            )
            for index_key in self.indices.keys():
                Indexer = self.indices[index_key]
                Indexer.ingest(wrapped_object)


    def ingest_rmd_collection(self, wrapped_collection):
        if not (wrapped_collection["data_type"] == "RMD_DATA_COLLECTION"):
            raise ValueError("Not an RMD Collection")
        assert isinstance(wrapped_collection["contents"], list)

        for rmd_obj in wrapped_collection["contents"]:
            if isinstance(rmd_obj, object) and rmd_obj["data_type"] == "RMD_DATA_OBJECT":
                self.ingest_rmd_object(rmd_obj)
            else:
                print("Warning: Non-conformant object found.")


    def get_object(self, object_id):
        assert isinstance(object_id, str)
        if object_id in self.object_store.keys():
            return self.object_store[object_id][1]
        else:
            raise ValueError("Object not known to this database")


    def get_wrapped_object(self, object_id):
        assert isinstance(object_id, str)
        if object_id in self.object_store.keys():
            obj_type = self.object_store[object_id][0]
            obj = self.object_store[object_id][1]
            return {
                "data_type": "RMD_DATA_OBJECT",
                "block_info": { # In future: block:trx:op and other props
                    "in_block": 5163946,
                    "trx_in_block": 2,
                    "op_in_trx": 0,
                    "block_time": "2023-09-19T12:00:00"
                },
                "object_type": obj_type,
                "object_id": object_id,
                "object": obj
            }
        else:
            raise ValueError("Object not known to this database")


    def get_objects_by_type_and_field(self, obj_type, field = None, value = None):
        result = []
        for o_id in self.object_store.keys():
            o_type = self.object_store[o_id][0]
            o = self.object_store[o_id][1]
            include_in_result = (
                (o_type == obj_type) and
                (field in o.keys() if field else True) and
                (o[field] == value if (field and value) else True)
            )
            result.append({
                "data_type": "RMD_DATA_OBJECT",
                "block_info": { # In future: block:trx:op and other props
                    "in_block": 5371387,
                    "trx_in_block": 0,
                    "op_in_trx": 0,
                    "block_time": "2023-08-25T12:00:00"
                },
                "object_type": o_type,
                "object_id": o_id,
                "object": o,
            }) if include_in_result else None
        return {
            "data_type": "RMD_DATA_COLLECTION",
            "contents": result
        }

    def get_index(self, index_key, **kwargs):
        for key in self.indices.keys():
            if key == index_key:
                return self.indices[key].get_index(**kwargs)
        raise ValueError("Unknown index type")


    def get_index_keys(self, index_key):
        return list(self.get_index(index_key).keys())


    def get_index_counts(self, index_key):
        ret_obj = {}
        index = self.get_index(index_key)
        for key in index.keys():
            ret_obj[key] = len(index[key])
        return ret_obj

    def get_unique_values(self, obj_type, obj_field):
        ret_list = []

        for key in self.object_store.keys():
            if self.object_store[key][0] == obj_type:
                val = self.object_store[key][1].get(obj_field, None)
                ret_list.append(
                    self.object_store[key][1].get(obj_field, None)
                ) if ((val is not None) and (val not in ret_list)) else None
        return ret_list


if __name__ == "__main__":
    # If invoked directly, run tests:
    import json

    unit_test_data = {}
    with open("./unit-test-rmd-db-02.json") as in_file:
        unit_test_data = json.loads(in_file.read())


    mdb = ModelDatabase()

    mdb.ingest_rmd_collection(unit_test_data)

    print(json.dumps(mdb.get_index("by_type")))
    print(json.dumps(mdb.get_index_keys("by_type")))
    print(json.dumps(mdb.get_index_counts("by_type")))
    print(json.dumps(mdb.get_index("by_jurisdiction")))
    print(json.dumps(mdb.get_unique_values("gaming_application", "jurisdiction")))

