#!/usr/bin/env python3
import json
import sys
from ipfs_cid import cid_sha256_hash

obj_type_map = {
    "party": "party_or_entity",
    "game": "gaming_activity_description",
    "application": "gaming_application",
    "report": "gaming_activity_report",
    "witness": "evidence_of_witness"
}

def encapsulate_dict(obj, obj_type):
    assert type(obj) == dict
    canon = get_canonical(obj)
    cid = get_cid(canon)

    out_obj = {
        'data_type': 'RMD_DATA_OBJECT',
        'object_type': obj_type,
        'object_id': cid,
        'object': obj
    }

    return out_obj

def get_canonical(in_obj):
    """ Encode a dict into canonical JSON bytes.  Canonical means no
    whitespace, no newlines, keys are lexically sorted, and bytes are
    utf-8 encoded.
    """
    return json.dumps(in_obj, separators=(',',':'), sort_keys=True).encode('utf-8')


def get_cid(in_bytes):
    result = cid_sha256_hash(in_bytes)
    return result


def replace_lazy_keys_generic(obj):
    """
    Replace any Lazy-Keys with appropriate ObectID.  Identifies a Lazy
    Key as any string field that begins with 'lazy:', and looks up
    what follows the ':' in the lazy_keys tabel.  Assumes the object
    referred to by the Lazy Key has already been processed and exists
    in the table. If not, a KeyError will promulgate.
    """

    # Recursive: Recurses over all elements if we are a dict or a
    # list, else (if we are string, numeric, or other) we do nothing
    # (ie we unwind).  Note: To edit strings in place we have to
    # access them by reference.  This means we have to check each
    # element of dict or list separately so we can us use [] operator
    # to get the ref. (I.e, we can't let a recursive call on a string
    # handle it b/c we lose the reference.)

    if isinstance(obj, dict):
        for key in obj.keys():
            if isinstance(obj[key], str):
                if obj[key].startswith("lazy:"):
                    obj[key] = lazy_keys[obj[key].split(':')[1]]
            else:
                replace_lazy_keys_generic(obj[key])
    elif isinstance(obj, list):
        for idx in range(len(obj)):
            if isinstance(obj[idx], str):
                if obj[idx].startswith("lazy:"):
                    obj[idx] = lazy_keys[obj[idx].split(':')[1]]
            else:
                replace_lazy_keys_generic(obj[idx])
    else: # numeric, string not found within a dict or list, or other
        pass


lazy_keys = {}

if __name__ == "__main__":

    in_args = sys.argv[1:]
    in_objects = []
    in_files = []
    dump_lazy_keys = False

    for arg in in_args:
        if arg.startswith('-'):
            if arg == "--dump-lazy-keys":
                dump_lazy_keys = True
        else:
            in_files.append(arg)

    for in_f in in_files:
        try:
            with open(in_f) as in_h:
                in_str = in_h.read()
                in_blob = json.loads(in_str)
                if type(in_blob)==dict:
                    in_blob = [in_blob]
            in_objects.extend(in_blob)
        except:
            print("Could not parse JSON from %s.\n"%in_f, file=sys.stderr)
            sys.exit(1)

    collection = []

    for element in in_objects:
        obj_content = element["object"]
        obj_type = element["type"]
        replace_lazy_keys_generic(obj_content)
        new_item = encapsulate_dict(
            obj_content,
            obj_type_map[obj_type]
        )
        if not new_item["object_id"] in lazy_keys.values():
            collection.append(new_item)
        if not element["lazy_key"] in lazy_keys.keys():
            lazy_keys[element["lazy_key"]] = new_item["object_id"]
        elif not new_item["object_id"]==lazy_keys[element["lazy_key"]]:
            print(
                "ERROR: Processing %s: Lazy key not unique: %s already points to %s." %
                (new_item["object_id"], element["lazy_key"], lazy_keys[element["lazy_key"]]),
                file=sys.stderr
            )
            sys.exit(1)


    out_blob = {
        'data_type': 'RMD_DATA_COLLECTION',
        'contents': collection
    }

    print(json.dumps(out_blob, separators=(',',':'), sort_keys=False))
    if dump_lazy_keys:
        print()
        print(json.dumps(lazy_keys, separators=(',',':'), sort_keys=False))
