#
import json
import sys

from bitshares.bitshares import BitShares
from bitsharesbase import operations
from bitsharesbase.objects import (
    ObjectId
)


API = 'wss://testnet.peerplays.download/api'
ACTOR = 'customop-test-01'
KEYS = ["5Hwb5cmVXZv9EPJuH9rHHH6YpjnqYVXZsMSSnwYYVUJVseXjz73"]
ID = 65535

chain = BitShares(API, keys=KEYS)

def construct_cust_operation(data_as_string, account):
    as_hex = data_as_string.encode('utf-8').hex()
    cust = operations.Custom(
        **{
            "fee": {"amount": 0, "asset_id": "1.3.0"},
            "payer": account["id"],
            "required_auths": [],
            "id": ID,
            "data": as_hex,
            "prefix": "TEST"
        }
    )
    return cust


if __name__ == '__main__':
    print("Hello")

    in_args = sys.argv[1:]
    in_objects = []
    in_files = []

    for arg in in_args:
        if arg.startswith('-'):
            pass # FUTURE: process switches here
        else:
            in_files.append(arg)

    for in_f in in_files:
        try:
            with open(in_f) as in_h:
                in_str = in_h.read()
                in_blob = json.loads(in_str)
                if type(in_blob)==dict:
                    in_blob = [in_blob]
            in_objects.extend(in_blob)
        except:
            print("Could not parse JSON from %s.\n"%in_f, file=sys.stderr)
            sys.exit(1)


    for element in in_objects:
        if ((not 'data_type' in element.keys()) or element['data_type'] != ('RMD_DATA_COLLECTION')):
            print(
                "ERROR: Not an RMD Data Collection.",
                file=sys.stderr
            )
            sys.exit(1)

    account = chain.account_class(ACTOR, blockchain_instance = chain)

    for element in in_objects:
        as_str = json.dumps(element, separators=(',',':'), sort_keys=False)
        cust = construct_cust_operation(as_str, account)
        print(chain.finalizeOp(cust, account, "active"))
