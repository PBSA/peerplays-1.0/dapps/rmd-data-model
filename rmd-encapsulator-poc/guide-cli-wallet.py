#!/usr/bin/env python3
import json
import sys

if __name__ == "__main__":

    in_args = sys.argv[1:]
    in_objects = []
    in_files = []
    dump_lazy_keys = False

    for arg in in_args:
        if arg.startswith('-'):
            pass # FUTURE: process switches here
        else:
            in_files.append(arg)

    for in_f in in_files:
        try:
            with open(in_f) as in_h:
                in_str = in_h.read()
                in_blob = json.loads(in_str)
                if type(in_blob)==dict:
                    in_blob = [in_blob]
            in_objects.extend(in_blob)
        except:
            print("Could not parse JSON from %s.\n"%in_f, file=sys.stderr)
            sys.exit(1)

    hex_blobs = []

    for element in in_objects:
        if ((not 'data_type' in element.keys()) or element['data_type'] != ('RMD_DATA_COLLECTION')):
            print(
                "ERROR: Not an RMD Data Collection.",
                file=sys.stderr
            )
            sys.exit(1)
        as_str = json.dumps(element, separators=(',',':'), sort_keys=False)
        as_hex = as_str.encode('utf-8').hex()
        hex_blobs.append(as_hex)

    for blob in hex_blobs:
        args = {
            "prompt": '>>> ',
            "handle": 0,
            "op_as_str": '[35,{"fee":{"amount":0,"asset_id":"1.3.0"},"payer":"%s","required_auths":[],"id":0,"data":"%s"}]'%("1.2.23", blob),
            "broadcast": 'true'
        }
        print("%(prompt)sbegin_builder_transaction" % args)
        print("%(prompt)sadd_operation_to_builder_transaction %(handle)s %(op_as_str)s" % args)
        print("%(prompt)sset_fees_on_builder_transaction %(handle)s 1.3.0" % args)
        print("%(prompt)ssign_builder_transaction %(handle)s %(broadcast)s" % args)
