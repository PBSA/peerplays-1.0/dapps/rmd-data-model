reduce inputs as $item ({}; . *= $item)
| . as $objects
| (
    $objects
    | keys
  )
  as $object_codes
| (
    $object_codes
    | map(
        {
          "if": {
            "properties": {"object_type": {"const": . }},
            "required": ["object_type"]
          },
          "then": {
            "properties": {
              "object": {
                "$ref": ("#/rmd_objects/" + .)
              }
            },
            "required": [
              "object"
            ]
          }
        }
      )
    | {"allOf": . }
  )
  as $rmd_object_selectable_type
| reduce . as $item (
    {"_classifier": {"enum":$object_codes}};
    . *= $item
  )
| . *= {"object_polymorphic_type": $rmd_object_selectable_type}
| {"rmd_objects": . }
